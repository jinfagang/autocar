import socket
import time
import sys


def run(ip):
    print('Socket ip is: ', ip)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((ip, 9999))# 注意bind的这里，IP地址和端口号都要与前面的程序中一样
    sock.listen(2)# 监听端口

    # 等待数据源端连接
    src, src_addr = sock.accept()
    print("Source Connected by", src_addr)
    # 等待目标端连接
    dst, dst_addr = sock.accept()
    print("Destination Connected by", dst_addr)

    while True:
        msg = src.recv(1024 *1024)
        if not msg:
            break
        try:
            dst.sendall(msg)
        except Exception as e:
            dst, dst_addr = sock.accept()
            print("Destination Connected Again by ", dst_addr, e)
        except KeyboardInterrupt:
            print("Interrupted")
            break

    src.close()
    dst.close()

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('provide an ip.')
    else:
        ip = sys.argv[1]
        run(ip)